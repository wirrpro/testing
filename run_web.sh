#!/bin/sh

sleep 20
cd squidward
su -m flasker -c "python manage.py db init"
sleep 10
su -m flasker -c "python manage.py db migrate"
sleep 10
su -m flasker -c "python manage.py db upgrade"


su -m flasker -c "python manage.py runserver -h 0.0.0.0 -p 8000"

