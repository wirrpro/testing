# import app
import os, sys
from datetime import datetime, date, timedelta, time
import postgres_copy
from sqlalchemy import text
from .. import db
from ..models import SquidFullData, Successes  # SquidFullData1 #, SquidIP, SquidSize
from .. import celery
# from flask import current_app


FILENAME = (os.path.basename(__file__)).split('.py')
SCRIPT_NAME = FILENAME[0]




@celery.task
def copy_csv_accesslog_async():
    with open('./logs/pstgr.csv') as fp:
        postgres_copy.copy_from(fp, SquidFullData, db.engine, format='csv',
                                    columns=('datetime', 'duration', 'ip', 'size',
                                             'resource'), header=False)
        write_report('Записи добавлены в базу', 1)
        # write_report('во время записи в базу произошла ошибка', 1)

    # try:
    #     create_last_one()
    #     create_last_two()
    #     create_last_three()
    #     create_last_four()
    #
    # except:
    #     pass

def create_last_one():
    weeknumber='squid_last_one'
    today = datetime.combine(date.today(), time())

    last_mon = today - timedelta(days=today.weekday(), weeks=1)
    last_mon = int(datetime.timestamp(last_mon))
    last_fri = last_mon + 432000

    from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)



def create_last_two():
    weeknumber='squid_last_two'

    today = datetime.combine(date.today(), time())

    last_mon = today - timedelta(days=today.weekday(), weeks=2)
    last_mon = int(datetime.timestamp(last_mon))
    last_fri = last_mon + 432000
    from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)


def create_last_three():
    weeknumber='squid_last_three'

    today = datetime.combine(date.today(), time())

    last_mon = today - timedelta(days=today.weekday(), weeks=3)
    last_mon = int(datetime.timestamp(last_mon))
    last_fri = last_mon + 432000
    from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)


def create_last_four():
    weeknumber='squid_last_four'

    today = datetime.combine(date.today(), time())

    last_mon = today - timedelta(days=today.weekday(), weeks=4)
    last_mon = int(datetime.timestamp(last_mon))
    last_fri = last_mon + 432000
    from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)


def from_full_to_weeks(startdata, stopdata, weeknumber):
    try:
        sql = text('''CREATE TABLE {0}
                    AS (SELECT ip, sum(size) as summa, resource
                    FROM squid_full
                    WHERE datetime BETWEEN {1} AND {2}
                    GROUP BY ip, resource
                    ORDER BY summa
                    DESC
                    LIMIT 100)'''.format(weeknumber, startdata, stopdata))
        db.engine.execute(sql)
        write_report('База {0} создана'.format(weeknumber), 1)

    except:
        write_report('во время создания {0} произошла ошибка'.format(weeknumber), 1)



def write_report(message, state, section=SCRIPT_NAME):
    date = datetime.now()
    result = Successes(datetime=date, section=section,
                       message=message, state=state)
    db.session.add(result)
    db.session.commit()


