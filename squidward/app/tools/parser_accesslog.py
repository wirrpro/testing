from datetime import datetime, date, timedelta, time
import csv
import os, sys
from urllib.parse import urlparse
from sqlalchemy import text
from .. import db
from . import delay_copy_squidcsv
from .. import celery
from ..models import Successes


FILENAME = (os.path.basename(__file__)).split('.py')
SCRIPT_NAME = FILENAME[0]


class BigLogNew:
    """Iterator, takes path_to_file_txt
    with Squid logs, gives formatting string
    for import to csv format, regexp support
    will be added
    """

    def __init__(self, file_txt, regexp=None):
        self.file_txt = file_txt
        self.regexp = regexp

    def __iter__(self):
        return self.parse_replace()

    def parse_replace(self):
        """
        :return:
        Logs prepared for convert to csv format
        """
        # re_line = re.compile(self.reg

        with open(self.file_txt, 'r') as f:
            for line in f:
                line = line.replace('\n', '')
                line = line.split()
                line_0 = int(float(line[0]))
                if int(line[1]) > 0:
                    check = urlparse(line[6]).hostname
                    if check == None:
                        line[6] = line[6].rsplit(':')[0]
                        line = line_0, line[1], line[2], line[4], line[6]
                    else:
                        line[6] = check
                        line = line_0, line[1], line[2], line[4], line[6]
                    yield line

                else:
                    pass

    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            print(exc_type, exc_value, traceback)
        return self

    def __str__(self):
        return 'BigLogRead({}, {})'.format(
            'file_txt', 'regexp')

    def __repr__(self):
        return "BigLogRead(file_txt='{}, 'regexp='{}')".format(
            self.file_txt, self.regexp)


class BigLogAdd:
    """Iterator, takes path_to_file_txt
    with Squid logs, gives formatting string
    for import to csv format, regexp support
    will be added
    """

    def __init__(self, file_txt, regexp=None):
        self.file_txt = file_txt
        self.regexp = regexp

    def __iter__(self):
        return self.parse_replace()

    def parse_replace(self):
        """
        :return:
        Logs prepared for convert to csv format
        """
        # re_line = re.compile(self.regexp)
        sql = text('''SELECT datetime from squid_full
                      order by datetime desc limit 1''')
        request = int((list(db.engine.execute(sql)))[0][0])
        if request:
            with open(self.file_txt, 'r') as f:
                for line in f:
                    line = line.replace('\n', '')
                    line = line.split()
                    line_0 = int(float(line[0]))
                    if line_0 > int(request[0]):
                        if int(line[1]) > 0:
                            check = urlparse(line[6]).hostname
                            if check == None:
                                line[6] = line[6].rsplit(':')[0]
                                line = line_0, line[1], line[2], line[4], line[6]

                            else:
                                line[6] = check
                                line = line_0, line[1], line[2], line[4], line[6]

                            yield line

                        else:
                            pass
                    else:
                        pass


    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            print(exc_type, exc_value, traceback)
        return self

    def __str__(self):
        return 'BigLogAdd({}, {})'.format(
            'file_txt', 'regexp')

    def __repr__(self):
        return "BigLogAdd(file_txt='{}, 'regexp='{}')".format(
            self.file_txt, self.regexp)



def find_accesslog():
    logdir = './logs/'
    filenames = []
    for root, _, files in os.walk(logdir):
        for file in files:
            if 'access' in file:
                filenames.append(root + file)
                print(file)
    return filenames


def create_csv_new():
    try:
        file_csv = './logs/pstgr.csv'
        with open(file_csv, 'w', newline='') as fcsv:
            for filename in find_accesslog():
                big = BigLogNew(filename)
                writer = csv.writer(fcsv)
                writer.writerows(big)
        write_report('csv готов', 1)

        delay_copy_squidcsv.copy_csv_accesslog_async()


    except:
        write_report('во время записи csv произошла ошибка', 1)


def create_csv_add():
    try:
        file_csv = './logs/pstgr.csv'
        with open(file_csv, 'w', newline='') as fcsv:
            for filename in find_accesslog():
                big = BigLogAdd(filename)
                writer = csv.writer(fcsv)
                writer.writerows(big)
        write_report('csv готов', 1)

        delay_copy_squidcsv.copy_csv_accesslog_async()


    except:
        write_report('во время записи csv произошла ошибка', 1)




#
# def create_last_one():
#     weeknumber='squid_last_one'
#     today = datetime.combine(date.today(), time())
#
#     last_mon = today - timedelta(days=today.weekday(), weeks=1)
#     last_mon = int(datetime.timestamp(last_mon))
#     last_fri = last_mon + 432000
#
#     from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)
#
#
#
# def create_last_two():
#     weeknumber='squid_last_two'
#
#     today = datetime.combine(date.today(), time())
#
#     last_mon = today - timedelta(days=today.weekday(), weeks=2)
#     last_mon = int(datetime.timestamp(last_mon))
#     last_fri = last_mon + 432000
#     from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)
#
#
# def create_last_three():
#     weeknumber='squid_last_three'
#
#     today = datetime.combine(date.today(), time())
#
#     last_mon = today - timedelta(days=today.weekday(), weeks=3)
#     last_mon = int(datetime.timestamp(last_mon))
#     last_fri = last_mon + 432000
#     from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)
#
#
# def create_last_four():
#     weeknumber='squid_last_four'
#
#     today = datetime.combine(date.today(), time())
#
#     last_mon = today - timedelta(days=today.weekday(), weeks=4)
#     last_mon = int(datetime.timestamp(last_mon))
#     last_fri = last_mon + 432000
#     from_full_to_weeks(startdata=last_mon, stopdata=last_fri, weeknumber=weeknumber)
#
#
# def from_full_to_weeks(startdata, stopdata, weeknumber):
#     try:
#         sql = text('''CREATE TABLE {0}
#                     AS (SELECT ip, sum(size) as summa, resource
#                     FROM squid_full
#                     WHERE datetime BETWEEN {1} AND {2}
#                     GROUP BY ip, resource
#                     ORDER BY summa
#                     DESC
#                     LIMIT 100)'''.format(weeknumber, startdata, stopdata))
#         db.engine.execute(sql)
#         write_report('База {0} создана'.format(weeknumber), 1)
#
#     except:
#         write_report('во время создания {0} произошла ошибка'.format(weeknumber), 1)



def write_report(message, state, section=SCRIPT_NAME):
    date = datetime.now()
    result = Successes(datetime=date, section=section,
                       message=message, state=state)
    db.session.add(result)
    db.session.commit()



@celery.task
def create_accesslog_csv_new_async():
    create_csv_new()
    # create_last_one()
    # create_last_two()
    # create_last_three()
    # create_last_four()


@celery.task
def create_csv_accesslog_add_async():
    create_csv_add()
    # create_last_one()
    # create_last_two()
    # create_last_three()
    # create_last_four()


