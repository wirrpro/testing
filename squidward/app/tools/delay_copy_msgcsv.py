# import app
import os, sys
from datetime import datetime
import postgres_copy
from .. import db
from ..models import WhiteToDB, GrayToDB, Successes
from .. import celery
# from flask import current_app


FILENAME = (os.path.basename(__file__)).split('.py')
SCRIPT_NAME = FILENAME[0]


@celery.task
def copy_csv_messages_async():
    try:
        with open('./logs/msg_w.csv') as fp:
            postgres_copy.copy_from(fp, WhiteToDB , db.engine, format='csv',
                                    columns=('datetime', 'pptpid', 'whiteip'), header=False)
        write_success('csv для whitelogs записан в базу данных', 1)

        with open('./logs/msg_g.csv') as fp:
            postgres_copy.copy_from(fp, GrayToDB , db.engine, format='csv',
                                    columns=('datetime', 'pptpid', 'grayip'), header=False)
        write_success('csv для graylogs записан в базу данных', 1)

    except:
        write_success('во время записи csv произошла ошибка', 1)


def write_success(message, state, section=SCRIPT_NAME):
    date = datetime.now()
    result = Successes(datetime=date, section=section,
                       message=message, state=state)
    db.session.add(result)
    db.session.commit()


