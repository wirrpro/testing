import os
import re
import time
from datetime import datetime
from sqlalchemy import text
import csv
from . import delay_copy_msgcsv
from .. import celery
from .. import db
from ..models import Successes


FILENAME = (os.path.basename(__file__)).split('.py')
SCRIPT_NAME = FILENAME[0]

class MessagesReadNew:
    """Iterator, takes path_to_file_txt
    with PoPToP logs, gives formatting string
    for import to csv format
    """

    def __init__(self, file_txt, type, regexp=None):
        self.file_txt = file_txt
        self.type = type
        self.regexp = regexp

    def __iter__(self):
        return self.parse_replace()

    def parse_replace(self):
        """
        :return:
        Logs prepared for convert to csv format
        """
        # re_line = re.compile(self.regexp)

        if self.type == 'white':
            with open(self.file_txt, 'r') as f:
                for line in f:
                    re_dateip = re.compile(
                        r'(.+\d+:\d+:\d+).+\[(.+)\].+?Client.*?(\d+\.\d+\.\d+\.\d+)')
                    eq_dateip = re_dateip.findall(line)
                    if eq_dateip:
                        white_date = eq_dateip[0][0]
                        white_id = eq_dateip[0][1]
                        white_ip = eq_dateip[0][2]
                        full_date = datetime.strptime(
                            time.strftime('%Y ') +
                            white_date, '%Y %b  %d %H:%M:%S')
                        full_date = str(full_date)
                        timestamp = time.mktime(time.strptime(
                            full_date, '%Y-%m-%d %H:%M:%S'))

                        yield timestamp, white_id, white_ip



        elif self.type == 'gray':
            with open(self.file_txt, 'r') as file:
                for line in file:
                    re_dateip = re.compile(
                        r'(.+\d+:\d+:\d+).+\[(.+)\].+?remote.*?(\d+\.\d+\.\d+\.\d+)')
                    eq_dateip = re_dateip.findall(line)
                    if eq_dateip:
                        gray_date = eq_dateip[0][0]
                        gray_id = int(eq_dateip[0][1]) - 1
                        gray_ip = eq_dateip[0][2]
                        full_date = datetime.strptime(
                            time.strftime('%Y ') +
                            gray_date, '%Y %b  %d %H:%M:%S')
                        full_date = str(full_date)
                        timestamp = time.mktime(time.strptime(
                            full_date, '%Y-%m-%d %H:%M:%S'))

                        yield timestamp, str(gray_id), gray_ip


        else:
            pass


    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            print(exc_type, exc_value, traceback)
        return self

    def __str__(self):
        return 'MessagesReadNew({}, {}, {})'.format(
            'file_txt', 'type', 'regexp')

    def __repr__(self):
        return "MessagesReadNew(file_txt='{}, type='{}', 'regexp='{}')".format(
            self.file_txt, self.regexp)


class MessagesReadAdd:
    """Iterator, takes path_to_file_txt
    with PoPToP logs, gives formatting string
    for import to csv format
    """

    def __init__(self, file_txt, type, regexp=None):
        self.file_txt = file_txt
        self.type = type
        self.regexp = regexp

    def __iter__(self):
        return self.parse_replace()

    def parse_replace(self):
        """
        :return:
        Logs prepared for convert to csv format
        """

        sql = text('''SELECT datetime from whitelogs
                      order by datetime desc limit 1''')
        request = int((list(db.engine.execute(sql)))[0][0])
        if request:
            if self.type == 'white':
                with open(self.file_txt, 'r') as f:
                    for line in f:
                        re_dateip = re.compile(
                            r'(.+\d+:\d+:\d+).+\[(.+)\].+?Client.*?(\d+\.\d+\.\d+\.\d+)')
                        eq_dateip = re_dateip.findall(line)
                        if eq_dateip:
                            white_date = eq_dateip[0][0]
                            white_id = eq_dateip[0][1]
                            white_ip = eq_dateip[0][2]
                            full_date = datetime.strptime(
                                time.strftime('%Y ') +
                                white_date, '%Y %b  %d %H:%M:%S')
                            full_date = str(full_date)
                            timestamp = time.mktime(time.strptime(
                            full_date, '%Y-%m-%d %H:%M:%S'))
                            if int(timestamp) > request:
                                yield timestamp, white_id, white_ip
                            else:
                                pass



            elif self.type == 'gray':
                with open(self.file_txt, 'r') as file:
                    for line in file:
                        re_dateip = re.compile(
                            r'(.+\d+:\d+:\d+).+\[(.+)\].+?remote.*?(\d+\.\d+\.\d+\.\d+)')
                        eq_dateip = re_dateip.findall(line)
                        if eq_dateip:
                            gray_date = eq_dateip[0][0]
                            gray_id = int(eq_dateip[0][1]) - 1
                            gray_ip = eq_dateip[0][2]
                            full_date = datetime.strptime(
                                time.strftime('%Y ') +
                                gray_date, '%Y %b  %d %H:%M:%S')
                            full_date = str(full_date)
                            timestamp = time.mktime(time.strptime(
                                full_date, '%Y-%m-%d %H:%M:%S'))
                            if int(timestamp) > request:
                                yield timestamp, str(gray_id), gray_ip

                            else:
                                pass



        else:
            write_report('Запрос к базе данных не удался', 1)


    def __exit__(self, exc_type, exc_value, traceback):
        if exc_type is not None:
            print(exc_type, exc_value, traceback)
        return self

    def __str__(self):
        return 'MessagesReadNew({}, {}, {})'.format(
            'file_txt', 'type', 'regexp')

    def __repr__(self):
        return "MessagesReadNew(file_txt='{}, type='{}', 'regexp='{}')".format(
            self.file_txt, self.regexp)


def find_accesslog():
    logdir = '/home/dds/squidward/app/tools/logs/'
    filenames = []
    for root, _, files in os.walk(logdir):
        for file in files:
            if 'messages' in file:
                filenames.append(root + file)
    if  len(filenames) == 0:
        write_report('каталог логов пуст', 1)
    else:
        return filenames


def write_csv_new():
    try:
        file_csv = '/home/dds/squidward/app/tools/logs/msg_w.csv'
        with open(file_csv, 'w', newline='') as fcsv:
            for filename in find_accesslog():
                msg = MessagesReadNew(filename, 'white')
                writer = csv.writer(fcsv)
                writer.writerows(msg)
        write_report('csv для whitelogs готов', 1)

        file_csv = '/home/dds/squidward/app/tools/logs/msg_g.csv'
        with open(file_csv, 'w', newline='') as fcsv:
            for filename in find_accesslog():
                msg = MessagesReadNew(filename, 'gray')
                writer = csv.writer(fcsv)
                writer.writerows(msg)
        write_report('csv для graylogs готов', 1)

        delay_copy_msgcsv.copy_csv_messages_async()

    except:
        write_report('во время записи csv произошла ошибка', 1)


def write_csv_add():
    try:
        file_csv = './logs/msg_w.csv'
        with open(file_csv, 'w', newline='') as fcsv:
            for filename in find_accesslog():
                msg = MessagesReadAdd(filename, 'white')
                writer = csv.writer(fcsv)
                writer.writerows(msg)
        write_report('csv для whitelogs готов', 1)

        file_csv = './logs/msg_g.csv'
        with open(file_csv, 'w', newline='') as fcsv:
            for filename in find_accesslog():
                msg = MessagesReadAdd(filename, 'gray')
                writer = csv.writer(fcsv)
                writer.writerows(msg)
        write_report('csv для graylogs готов', 1)

        delay_copy_msgcsv.copy_csv_messages_async()

    except:
        write_report('во время записи csv произошла ошибка', 1)





def write_report(message, state, section=SCRIPT_NAME):
    date = datetime.now()
    result = Successes(datetime=date, section=section,
                       message=message, state=state)
    db.session.add(result)
    db.session.commit()



@celery.task
def create_csv_messages_new_assync():
    write_csv_new()



@celery.task
def create_csv_messages_add_async():
    write_csv_add()

