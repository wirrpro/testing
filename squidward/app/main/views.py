from flask import render_template as rt
from . import main


@main.route('/')
def index():
    """First Page. Very Beautiful."""
    return rt('index.html')
