from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from . import db, login_manager


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


class User(UserMixin, db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(), index=True)
    password_hash = db.Column(db.String())

    def __repr__(self):
        return 'User {0}'.format(self.username)

    @property
    def password(self):
        raise AttributeError('password is not readable attribute')

    @password.setter
    def password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)


class WhiteToDB(db.Model):
    __tablename__ = 'whitelogs'
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.Float())
    pptpid = db.Column(db.Integer())
    whiteip = db.Column(db.String())

    def __repr__(self):
        return 'WhiteToPostgres {0} {1} {2}'.format(
            self.datetime, self.pptpid, self.whiteip)


class GrayToDB(db.Model):
    __tablename__ = 'graylogs'
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.Float())
    pptpid = db.Column(db.Integer())
    grayip = db.Column(db.String())

    def __repr__(self):
        return 'GrayToPostgres {0} {1} {2}'.format(
            self.datetime, self.pptpid, self.grayip)



class SquidFullData(db.Model):
    __tablename__ = 'squid_full'
    id = db.Column(db.Integer(), primary_key=True)
    # datetime = db.Column(db.BigInteger(), index=True)
    datetime = db.Column(db.BigInteger(), index=True)
    duration = db.Column(db.Integer())
    ip = db.Column(db.String())
    size = db.Column(db.BigInteger())
    resource = db.Column(db.String())
    
    def __repr__(self):
        return 'SquidFullData {0} {1} {2} {3} {4}'.format(
                self.datetime, self.duration, self.ip, self.size, self.resource)


# class SquidLastOneWeeklyData(db.Model):
#     __tablename__ = 'squid_week_one'
#     id = db.Column(db.Integer(), primary_key=True)
#     datetime = db.Column(db.BigInteger())
#     duration = db.Column(db.Integer())
#     ip = db.Column(db.String())
#     size = db.Column(db.BigInteger())
#
#
# class SquidLastTwoWeeklyData(db.Model):
#     __tablename__ = 'squid_week_two'
#     id = db.Column(db.Integer(), primary_key=True)
#     datetime = db.Column(db.BigInteger())
#     duration = db.Column(db.Integer())
#     ip = db.Column(db.String())
#     size = db.Column(db.BigInteger())
#
#
# class SquidLastThreeWeeklyData(db.Model):
#     __tablename__ = 'squid_week_three'
#     id = db.Column(db.Integer(), primary_key=True)
#     datetime = db.Column(db.BigInteger())
#     duration = db.Column(db.Integer())
#     ip = db.Column(db.String())
#     size = db.Column(db.BigInteger())
#
#
# class SquidLastFourWeeklyData(db.Model):
#     __tablename__ = 'squid_week_four'
#     id = db.Column(db.Integer(), primary_key=True)
#     datetime = db.Column(db.BigInteger())
#     duration = db.Column(db.Integer())
#     ip = db.Column(db.String())
#     size = db.Column(db.BigInteger())


class Successes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.String())
    section = db.Column(db.String())
    message = db.Column(db.String)
    state = db.Column(db.Integer())

    def __repr__(self):
        return 'Successes {0} {1} {2} {3}'.format(
            self.datetime, self.section, self.message, self.state)

#
# class Errors(db.Model):
#     __tablename_ = 'errors_bd'
#     id = db.Column(db.Integer, primary_key=True)
#     datetime = db.Column(db.Float())
#     section = db.Column(db.String())
#     message = db.Column(db.String())
#     state = db.Column(db.Integer())
#
#     def __repr__(self):
#         return 'Errors {0} {1} {2} {3}'.format(
#             self.datetime, self.section, self.message, self.state)


