from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField,\
    BooleanField, validators, ValidationError
from ..models import User


class LoginForm(FlaskForm):
    """Login Form"""
    username = StringField('Имя пользователя',
                           [validators.DataRequired(),
                            validators.Length(min=3, max=16,
                                              message='Длина логина должна быть от\
                                           3 до 16 символов')],
                           render_kw={"placeholder": "Логин"})
    password = PasswordField('Пароль',
                             [validators.DataRequired(),
                              validators.Length(min=6, max=32,
                                                message='Длина пароля должна\
                                                быть от 6 до 32 символов')],
                             render_kw={"placeholder": "Пароль"})
    remember_me = BooleanField('Запомнить меня')
    submit = SubmitField('Войти')


class RegisterForm(FlaskForm):
    """Register Form"""
    username = StringField('Имя пользователя',
                           [validators.DataRequired(),
                            validators.Length(min=3, max=16,
                                              message='Длина логина должна быть от\
                                           3 до 16 символов')],
                           render_kw={"placeholder": "Логин"})
    password = PasswordField('Пароль',
                             [validators.DataRequired(),
                              validators.Length(min=6, max=32,
                                                message='Длина пароля должна\
                                                быть от 6 до 32 символов'),
                              validators.EqualTo('confirm',
                                                 message='Пароли должны\
                                                 совпадать')],
                             render_kw={"placeholder": "Пароль"})
    confirm = PasswordField('Подтвердите пароль')
    submit = SubmitField('Зарегистрироваться')

    def validate_username(self, field):
        if User.query.filter_by(username=field.data).first():
            raise ValidationError(
                'Пользователь с таким логином уже существует!')
