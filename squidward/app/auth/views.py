import os
from datetime import datetime
from flask import redirect, url_for, flash, render_template as rt
from flask_login import login_user, logout_user, login_required
from . import auth
from .. import db
from ..models import User, Successes
from .forms import LoginForm, RegisterForm


FILENAME = (os.path.basename(__file__)).split('.py')
SCRIPT_NAME = FILENAME[0]


@auth.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is not None and user.verify_password(form.password.data):
            login_user(user, form.remember_me.data)
            flash('Вы успешно залогинились')
            write_report('Пользователь {0} вошел в систему'.format(form.username.data), 1)

            return redirect(url_for('main.index'))
        else:
            flash('Неправильное имя или пароль')
    return rt('auth/login.html', form=form)


@auth.route('logout')
@login_required
def logout():
    logout_user()
    flash('Вы разлогинились.')

    return redirect(url_for('main.index'))


@auth.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,
                    password=form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Вы успешно зарегистрировались')
        write_report('Завершена регистрация пользователя {0}'.format(user), 1)
        return redirect(url_for('auth.login'))
    return rt('auth/register.html', form=form)


def write_report(message, state, section='Auth'):
    date = datetime.now()
    result = Successes(datetime=date, section=section,
                       message=message, state=state)
    db.session.add(result)
    db.session.commit()