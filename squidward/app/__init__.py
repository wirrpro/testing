from flask import Flask
from flask_bootstrap import Bootstrap
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from config import config, Config
from celery import Celery


bootstrap = Bootstrap()
db = SQLAlchemy()

login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'auth.login'
login_manager.login_message = 'Для доступа к этому разделу необходимо\
                               авторизоваться'


celery = Celery(__name__, broker=Config.CELERY_BROKER_URL)


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    db.init_app(app)

    # LoginManager
    login_manager.init_app(app)

    # Celery
    celery.conf.update(app.config)

    # errors routes and messages
    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    # authentication - login
    from .auth import auth as auth_blueprint
    app.register_blueprint(auth_blueprint, url_prefix='/auth')

    # poptoplogs
    from .pptp import pptp as pptp_blueprint
    app.register_blueprint(pptp_blueprint, url_prefix='/pptp')

    # squidward
    from .squidward import squidward as squidward_blueprint
    app.register_blueprint(squidward_blueprint, url_prefix='/squidward')

    return app
