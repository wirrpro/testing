from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, validators




class ButtonNew(FlaskForm):
    button_new = SubmitField('Первая установка')

class ButtonAdd(FlaskForm):
    button_add = SubmitField('Обновить базу')

class ButtonCsv(FlaskForm):
    button_csv = SubmitField('Из CSV')

class RemoveErrors(FlaskForm):
    button_remove = SubmitField('Удалить сообщения')

class MarkErrors(FlaskForm):
    button_mark = SubmitField('Пометить как прочитанные')
