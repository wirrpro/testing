import os
from datetime import datetime, date, timedelta, time
from flask import redirect, flash, render_template as rt
from flask_login import login_required
from sqlalchemy import text
from . import squidward
from .forms import ButtonNew, ButtonAdd, ButtonCsv,\
    MarkErrors, RemoveErrors
from .. import db
from ..models import Successes
from ..tools import parser_accesslog, delay_copy_squidcsv
from .. import celery


FILENAME = (os.path.basename(__file__)).split('.py')
SCRIPT_NAME = FILENAME[0]


@squidward.route('/', methods=['GET', 'POST'])
@login_required
def main():
    return rt('squidward/main.html')


@squidward.route('/month', methods=['GET', 'POST'])
@login_required
def month():
    dates_in_months = {}
    today = datetime.combine(date.today(), time())

    last_mon = today - timedelta(days=today.weekday(), weeks=1)
    last_mon = int(datetime.timestamp(last_mon))

    start_log_day_from_sql = text('''SELECT datetime
                        from squid_full 
                        order by datetime ASC limit 1
                        ''')
    start_log_day = int((list(db.engine.execute(start_log_day_from_sql)))[0][0])
    # start_log_day = 1507928400

    start_log_day = datetime.fromtimestamp(start_log_day)
    midnight_start_log_day = start_log_day.strftime('%Y%m%d')
    midnight_start_log_day = datetime.strptime(midnight_start_log_day, '%Y%m%d')

    n = 86400
    start = int(midnight_start_log_day.timestamp())
    while start <= last_mon:
        month = datetime.fromtimestamp(start).month
        weekday = datetime.fromtimestamp(start).isoweekday()
        human_day = datetime.fromtimestamp(start).day
        ts_day = int(datetime.fromtimestamp(start).timestamp())
        day_coord = (month, weekday, human_day, ts_day)
        start += n

        if month in dates_in_months:
            dates_in_months[month].append(day_coord)
        else:
            dates_in_months[month] = [day_coord]

    #  Костыль. Таблица заполняется с начала строки, т.е. игнорируется
    #  пустое значение, даты в таблице сдвигаются влево
    #  пока не разберусь с фронтом, будет так

    for month, days in dates_in_months.items():
        num_fakes = int(days[0][1] - 1)
        if num_fakes > 0:
            # if days start not from monday (1)
            fake_cells = [(13, x, 32, num_fakes) for x in range(1, num_fakes + 1)]
            for n in reversed(fake_cells):
                dates_in_months[month].insert(0, n)


    return rt('squidward/month.html', dates_in_months=dates_in_months)

@squidward.route('/day/<start>_<stop>', methods=['GET', 'POST'])
@login_required
def daily_report(start, stop):
    sql = text('''SELECT ip, resource, trunc(sum(size)/1024000) as sum
                  FROM squid_full
                  WHERE datetime BETWEEN {0} AND {1}
                  GROUP BY squid_full.ip, resource
                  ORDER BY sum DESC LIMIT 50'''.format(start, stop))

    report = db.engine.execute(sql)



    return rt('squidward/day.html', report=report,
              start=start, stop=stop)


@squidward.route('/day/<start>_<stop>/<user_ip>', methods=['GET', 'POST'])
@login_required
def daily_report_user(start, stop, user_ip):
    sql = text("""SELECT resource, trunc(sum(size)/1024000, 3) as sum
                  FROM  squid_full
                  WHERE datetime BETWEEN {0} AND {1}
                  and ip = '{2}'
                  GROUP BY resource
                  ORDER BY sum DESC LIMIT 100
                  """.format(start, stop, str(user_ip)))
    report = db.engine.execute(sql)

    return rt('squidward/day_user.html', report=report)

@squidward.route('/week', methods=['GET', 'POST'])
@login_required
def week():
    today = datetime.combine(date.today(), time())
    last_mon = today - timedelta(days=today.weekday(), weeks=1)
    last_mon = int(datetime.timestamp(last_mon))
    last_tue = last_mon + 86400
    last_wed = last_tue + 86400
    last_thi = last_wed + 86400
    # last_fri = last_mon + 432000
    last_fri = last_thi + 86400

    sql = text('''SELECT ip, resource,
        trunc(sum(size)/1024000) as sum
        FROM squid_full
        WHERE datetime BETWEEN {0} AND {1}
        GROUP BY squid_full.ip, resource
        ORDER BY sum DESC LIMIT 25'''.format(last_mon, last_tue))
    to_db = db.engine.execute(sql)


    return rt('squidward/week.html', to_db=to_db)


@squidward.route('/period', methods=['GET', 'POST'])
@login_required
def period():
    return rt('squidward/period.html')


@squidward.route('/user', methods=['GET', 'POST'])
@login_required
def user():
    return rt('squidward/user.html')


@squidward.route('/copy', methods=['GET', 'POST'])
@login_required
def copy():
    button_new = ButtonNew()
    button_add = ButtonAdd()
    button_csv = ButtonCsv()
    if button_new.button_new.data and button_new.validate():
        parser_accesslog.create_accesslog_csv_new_async.delay()
        flash('База данных обновляется')
        return redirect('squidward/copy')

    if button_add.button_add.data and button_add.validate():
        parser_accesslog.create_csv_accesslog_add_async.delay()
        flash('База данных обновляется')
        return redirect('squidward/copy')

    if button_csv.button_csv.data and button_csv.validate():
        delay_copy_squidcsv.copy_csv_accesslog_async.delay()
        flash('База данных обновляется')
        return redirect('squidward/copy')

    return rt('squidward/copy.html', form_new=button_new,
              form_add=button_add, form_csv=button_csv)


@squidward.route('/reports', methods=['GET', 'POST'])
@login_required
def reports():
    sql = Successes.query.with_entities(Successes.id,
                                        Successes.datetime,
                                        Successes.section,
                                        Successes.message,
                                        Successes.state).order_by(
                                            Successes.datetime.desc())
    button_remove = RemoveErrors()
    button_mark = MarkErrors()

    if button_remove.button_remove.data and button_remove.validate():
        remove_reports_async()
        return redirect('squidward/reports')

    if button_mark.button_mark.data and button_mark.validate():
        mark_reports_async()
        return redirect('squidward/reports')

    return rt('squidward/reports.html', to_db=sql, button_mark=button_mark,
              button_remove=button_remove)


@celery.task
def mark_reports_async():
    Successes.query.filter_by(state=1).update({'state': 0})
    db.session.commit()

@celery.task
def remove_reports_async():
    Successes.query.delete()
    db.session.commit()
