from flask import Blueprint

squidward = Blueprint('squidward', __name__)

from . import views, forms
from .. import tools
