from flask import redirect, flash,\
    render_template as rt
from flask_login import login_required
from sqlalchemy import func
from . import pptp
from .forms import SearchIPForm, ButtonNew, ButtonAdd
# from . import logs_test
from ..tools import parser_messages
from .. import db
from ..models import WhiteToDB
from sqlalchemy import text
from datetime import datetime
from .. import celery

import app


@pptp.route('/', methods=['GET', 'POST'])
@login_required
def poptop():
    form = SearchIPForm()
    result = 'Результаты появятся тут'
    try:
        entries = db.session.query(WhiteToDB).count()
        first_entry = db.session.query(func.min(WhiteToDB.datetime))
        last_entry = db.session.query(func.max(WhiteToDB.datetime))
        first_entry = datetime.fromtimestamp(first_entry.all()[0][0])
        last_entry = datetime.fromtimestamp(last_entry.all()[0][0])
    except:
        result = 'База данных, возможно, пуста'
        entries = ''
        first_entry = ''
        last_entry = ''
        first_entry = ''
        last_entry = ''
    if form.validate_on_submit():
        ip_address = form.ip_address.data
        date_human_from = form.date_from.data
        date_human_to = form.date_to.data
        # date_from to timestamp
        try:
            hstrp_date_from = datetime.strptime(date_human_from, '%d.%m.%Y')
            date_ts_from = hstrp_date_from.timestamp()

            if date_human_to == date_human_from:
                date_ts_to = date_ts_from + 86400
            elif date_human_to == '':
                date_ts_to = date_ts_from + 86400
            else:
                hstrp_date_to = datetime.strptime(date_human_to, '%d.%m.%Y')
                date_ts_to = hstrp_date_to.timestamp()


            sql = text('''
                SELECT
                whitelogs.datetime,
                whitelogs.whiteip,
                graylogs.grayip
                FROM graylogs INNER JOIN whitelogs ON
                graylogs.pptpid = whitelogs.pptpid
                WHERE graylogs.datetime BETWEEN {0} AND {1}
                AND graylogs.grayip = '{2}'
                AND graylogs.datetime - whitelogs.datetime BETWEEN 1 AND 15
                '''.format(date_ts_from, date_ts_to, ip_address))

            to_db = db.engine.execute(sql)
            result = 'Адрес {} отсутствует в базе, либо в указанную дату активность отсутствовала \n'.format(ip_address)
            to_db = sorted(to_db)
            if to_db:
                result = ''
            for query in to_db:
                row = datetime.fromtimestamp(query[0]).strftime(
                '%d.%m.%Y %H:%M:%S')
                result_str = row + ' ' + query[1]
                result += result_str + '\n'
        except:
            result = 'Проверьте правильность заполнения полей. ' \
                     'Возможно, вы допустили ошибку'
            return rt('pptp/pptp.html', form=form, result=result,
              entries=entries, first_entry=first_entry,
              last_entry=last_entry)
            
    return rt('pptp/pptp.html', form=form, result=result,
              entries=entries, first_entry=first_entry,
              last_entry=last_entry)


@pptp.route('/add_pptp_db', methods=['GET', 'POST'])
@login_required
def add_pptp_db():
    button_new = ButtonNew()
    button_add = ButtonAdd()
    if button_new.button_new.data and button_new.validate():
        parser_messages.create_csv_messages_new_assync.delay()
        flash('База данных обновляется')
        return redirect('/pptp')

    if button_add.button_add.data and button_add.validate():
        parser_messages.create_csv_messages_add_async.delay()
        flash('База данных обновляется')
        return redirect('/pptp')


    return rt('pptp/add_pptp_db.html', form_new=button_new,
              form_add=button_add)

