from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, validators


class SearchIPForm(FlaskForm):
    """Form for 1 ip address."""
    message = 'IPv4 адрес в формате XX.XX.XX.XX'
    ip_address = StringField(
        'Введите IP',
        [validators.DataRequired(),
         validators.IPAddress(ipv4=True, message=message)],
        render_kw={'placeholder': '172.20.0.1'})
    date_from = StringField('Дата с:',
                            [validators.DataRequired()],
                            render_kw={'placeholder': '11.07.1988'})
    date_to = StringField('Дата до:',

                          render_kw={'placeholder': '11.09.2001'})
    submit = SubmitField('Проверить')


class ButtonNew(FlaskForm):
    button_new = SubmitField('Первая установка')

class ButtonAdd(FlaskForm):
    button_add = SubmitField('Обновить базу')
