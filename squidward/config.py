import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = 'hoody boody'  # CHANGE
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_RECORD_QUERIES = True
    CELERY_BROKER_URL = 'amqp://celery:celerypassword@rabbit:5672/pptp'

    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    DEBUG = None
    SQLALCHEMY_DATABASE_URI = os.environ.get('PROD_DATABASE_URI') or \
        'postgresql://'


class TestConfig(Config):
    TESTING = True
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URI') or \
        'postgresql://'


class DevelopmentConfigWork(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URI') or \
        'postgresql://squidward:Parolsquidward#@db/pptp'


class DevelopmentConfigHome(Config):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'postgresql://squidward:Parolsquidward#@db/pptp'


config = {
    'development_work': DevelopmentConfigWork,
    'development_home': DevelopmentConfigHome,
    'production': ProductionConfig,

    'default': DevelopmentConfigWork
}

LOG_DIR = {
    'development_work': './logs/',
    'development_home': './logs/',

}
LOG_DIR_ACCESS = {
    'development_work': './logs/',
    'development_home': './logs'
}
